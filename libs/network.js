"use strict";

const dgram = require('dgram');
const packetGenerator=require('./packet_generator');

// const client = dgram.createSocket("udp4");

// client.on('message',function(msg,info){
//   console.log('Data received from server : ' + msg.toString());
//   console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);
// });

/**
* Send the packet Over UDP
* @param {Object} networkSettings The network settings on where to send the packet
* @param {String} semtechPacket The packet payload
* @param {Function} callback Returning the resultd
*/
const sendSemtechPackage = function(networkSettings,semtechPacket,callback){
  const client = dgram.createSocket("udp4");
  client.send(semtechPacket, 0, semtechPacket.length, networkSettings.port, networkSettings.host, function(err, bytes) {
    if (err) {
      callback(err);
      client.close();
    }
    callback(null,semtechPacket,networkSettings.host,networkSettings.port);
    client.close();
  });
}

/**
* Send a payload afyer a specific time defined in timeout
* @param {Object} loraSettings The settings for LoraWan packet generation
* @param {Object} networkSettings The network settings on where to send the packet
* @param {String} initialPayload String Payload that will get transmitted via LoRa
* @param {Int} timeout When the new packet will get generated
* @param {Function} callback Callback that returns the transfet status
* @param {payloadUpdate} payloadUpdate SYNCRONOUS FUNCTION that renews the payload. (optional)
*/
const sceduledPacketSend=function(loraSettings,networkSettings,initialPayload,timeout,callback,payloadUpdateCallback){
  const nextStepLogic=(newPayload,next)=>{
      sendSemtechPackage(networkSettings,newPayload,(err,data,host,port)=>{
        if(err){
          return callback(err);
        }

        callback(null,data,host,port);
        next();
      })
  }
  packetGenerator.sequenctialPacketGenerator(loraSettings.appSKey,loraSettings.ntwSKey,loraSettings.nodeId,networkSettings.identifier,initialPayload,timeout,nextStepLogic,payloadUpdateCallback);
}

module.exports={
  'sendSemtechPackage':sendSemtechPackage,
  'sceduledPacketSend':sceduledPacketSend
}
