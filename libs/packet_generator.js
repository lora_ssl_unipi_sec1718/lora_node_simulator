"use strict";

const lora_packet = require('lora-packet');
const crypto = require('crypto');

//LoraWan pakcer counter
let counter=0;

/**
* Constructing the packet and return the base64 format of it
* @param {String} appSKey Application Session key
* @param {String} nwkSKey Network Session key
* @param {String} devAddr Device Address
* @param {String} payload String Payload that will get transmitted via LoRa
*/
const constructLoRaWanPacket=function(appSKey,nwkSKey,devAddr,payload){
  const packet= lora_packet.fromFields({
        MType: 'Unconfirmed Data Up',   // (default)
        DevAddr: new Buffer(devAddr, 'hex'), // big-endian
        FCtrl: {
            ADR: false,       // default = false
            ACK: true,        // default = false
            ADRACKReq: false, // default = false
            FPending: false   // default = false
        },
        FCnt: counter, // can supply a buffer or a number
        payload: payload
    }
    , new Buffer(appSKey, 'hex') // AppSKey
    , new Buffer(nwkSKey, 'hex') // NwkSKey
  );

  //We increase counter here to ensure that no same counter will get ransmitted
  counter++;

  return packet;
}


/**
* Generate the "radio" value of the recieved node to export into the
* @param {Buffer} loraPacket A buffer containing the packet constructed according to LoraWan specs.
* @return {String} with the final packet Json encoded
*/
const constructSemtechLoRaDataPayload=function(loraPacket,gatewayIdentifier){

  //Random Token
  const randomToken=crypto.randomBytes(2);

  //Allocating Bytes for address
  gatewayIdentifier=new Buffer(gatewayIdentifier,'hex');

  //Semtech JSON Payload
  const date = new Date();
  const packet=loraPacket.getPHYPayload();

  let retPacket= {
    "stat":{
      "time":date.toISOString(),
      "lati":0.0,
      "long":0.0,
      "alti":0,
      "rxnb":0,
      "rxok":0,
      "rxfw":0,
      "ackr":0.0,
      "dwnb":0,
      "txnb":0,
      "pfrm":"Single Channel Gateway",
      "mail":"",
      "desc":""
    },
    'rxpk':[{
  		"time":date.toISOString(),
  		"tmst":date.getTime(),
  		"chan":2,
  		"rfch":0,
  		"freq":866.349812,
  		"stat":1,
  		"modu":"LORA",
  		"datr":"SF7BW125",
  		"codr":"4/6",
  		"rssi":-35,
  		"lsnr":5.1,
  		"size": packet.length,
  		"data": packet.toString('base64')
    }]
  };

   retPacket=new Buffer(JSON.stringify(retPacket),'utf8');

   //Putting all together
   const finalPacket=Buffer.concat([new Buffer('0x02','hex'),randomToken,new Buffer('0x00','hex'),gatewayIdentifier,retPacket]);
   return finalPacket;
}

/**
* Constructing the final packet that will get sent to the
* @param {String} appSKey Application Session key
* @param {String} nwkSKey Network Session key
* @param {String} devAddr Device Address
* @param {String} gatewayIdentifier Identifier of the gateway
* @param {String} initialPayload String Payload that will get transmitted via LoRa
* @param {Int} timeout When the new packet will get generated
* @param {Function} callback Callback that returns the final packet
* @param {payloadUpdate} payloadUpdate SYNCRONOUS FUNCTION that renews the payload. (optional)
*
* NOTE: The `callback` takes 2 parameters:
* - finalPacket: That contains the value of the final packet
* - recallCallback: function WITHOUT parameters that generates the new packet with the new payload
*/
const sequenctialPacketGenerator=function(appSKey,nwkSKey,devAddr,gatewayIdentifier,initialPayload,timeout,callback,payloadUpdate){
  return setTimeout(()=>{
    const finalPacket=constructSemtechLoRaDataPayload(constructLoRaWanPacket(appSKey,nwkSKey,devAddr,initialPayload),gatewayIdentifier);
    return callback(finalPacket,()=>{
      let newPayload = initialPayload;
      if(payloadUpdate){
           newPayload = payloadUpdate(initialPayload);
      }
      return sequenctialPacketGenerator(appSKey,nwkSKey,devAddr,gatewayIdentifier,newPayload,timeout,callback,payloadUpdate)
    });
  },timeout);
}

module.exports={
  'constructSemtechLoRaDataPayload':constructSemtechLoRaDataPayload,
  'constructLoRaWanPacket':constructLoRaWanPacket,
  'sequenctialPacketGenerator':sequenctialPacketGenerator
}
