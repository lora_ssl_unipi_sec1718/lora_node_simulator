"use strict";

const fs=require('fs');

/**
* Load the settings from a Json File
* @param {String} path The path of the file containing json
* @param {Function} callback function that returns the result
*/
const loadJsonContentFromFile=function(path,callback){
    fs.readFile(path, 'utf8', function (err, data) {
      if (err){
        if(err.code === 'EACCESS') {
          return callback(
            new Error(`Your user has not the correct permissions to read the file: ${path}`)
          );
        }

        if(err.code === 'ENOENT') {
            return callback(
              new Error(`The file ${path} does not exist`)
            );
        }
      }
      try {
        const obj = JSON.parse(data);
        return callback(null,obj);
      } catch(error){
        return callback(new Error(`The file ${path} does not contain valid JSON content`));
      }
    });
}

/**
* Validating the parameters read from a de-serialized json content.
* Recommended use is as a callback. Keep in mind that also is used for **error handling** too.
* @param {Error} err A possible error occured
* @param {Object} params The parameters from a parsed Json content
* @param {Function} callback The rallback that received the error and the parameters.
*
* The **callback** takes the following 3 arguments:
* 1. {Error} err : The error during parsing and validating the arguments
* 2. {Object} loraWanParams: Parameters that contain the settings to generate the Lora Wan Payload:
*    - appSKey: The application Session key
*    - ntwSKey: The network Session Key
*    - nodeId: The network Id
* 3. {Object} networkParams: Parameters used in configuring and initializing the semtech udp connection to the network server.
*    It will contain the following parameters:
*    - {String} host: The host to connect
*    - {Numeric} port: The port to connect
*/
const validateJsonParams=function(err,params,callback){

  if(err){
    return callback(err);
  }

  let loraWanParams=null;
  if(params.lorawan && params.lorawan.appSKey && params.lorawan.ntwSKey && params.lorawan.nodeId){
    loraWanParams=params.lorawan;
  } else if(!params.lorawan) {
    return callback(new Error("No lorawan parameters provided"));
  } else if(!params.lorawan.appSKey) {
    return callback(new Error("No lorawan application session key provided"));
  } else if(!params.lorawan.ntwSKey) {
    return callback(new Error("No lorawan network session key provided"));
  } else if(!params.lorawan.nodeId) {
    return callback(new Error("No lorawan id for the node provided"));
  }

  let semtechUdpNetworkParameters={
    'host':'52.169.76.203',
    'port':1700,
    'default':{'host':true,'port':true}
  };

  if(params.udp && params.udp.identifier){
    semtechUdpNetworkParameters.identifier=params.udp.identifier;

    if(params.udp.host){
      semtechUdpNetworkParameters.default.host=false
      semtechUdpNetworkParameters.host=params.udp.host
    }

    if(params.udp.port && params.udp.port>=1 && params.udp.port<=65535) {
      semtechUdpNetworkParameters.default.port=false;
      semtechUdpNetworkParameters.port=params.udp.port
    } else if(params.udp.port && params.udp.port<1 || params.udp.port > 65535){
      return callback(new Error("Invalid port setting"));
    }
  }
  callback(null,loraWanParams,semtechUdpNetworkParameters);
}

/**
* Check if a path is a readable file
* @param {String} value The path of the file
* @return { String | Boolean } Return the path if all green
*         return false if something is fishy.
*/
const readableFile=function(value){
  const fs = require('fs');
  try {
    fs.accessSync(value,fs.R_OK);
    if(fs.lstatSync(value).isFile()){
      return value;
    } else {
      return false;
    }
  } catch(err) {
    console.error(err);
    return false;
  }
}

/**
* Read the settings from a file and validate the parameters
* @param {String} path The path of the file containing json
* @param {Function} callback function that returns the result
* **NOTE** keep in mind that the callback is the one used in `validateJsonParams`
*/
const readSettings= function(path,callback){
  loadJsonContentFromFile(path,(err,params)=>{
    return validateJsonParams(err,params,callback);
  })
}

module.exports={
  'readSettings': readSettings,
  'readableFile': readableFile
}
