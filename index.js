"use strict";

const settingsManager = require('./libs/settingsManager');
const network = require('./libs/network');
const ArgumentParser = require('argparse').ArgumentParser;
const chalk = require('chalk');

const parser = new ArgumentParser({
  version: '1.0.0',
  addHelp:true,
  description: 'LoraWan node-gateway connection simulator'
});

const printError=function(msg){
  console.error(chalk.red(msg));
}

parser.addArgument('params_file',{
    help: "The file containing the configuration regarding the abp activation keys and where to send the data via semtech udp protcol",
    type: settingsManager.readableFile
});

let args = parser.parseArgs();

settingsManager.readSettings(args.params_file, (err,loraWanParams,networkParams)=>{
  if(err){
    return printError(err.message);
  }

  // console.log(loraWanParams);
  // console.log(networkParams);

  network.sceduledPacketSend(loraWanParams,networkParams,"hello",1000,(error,data,host,port)=>{
    if(error){
      printError(error);
    }

    console.log(chalk.green(`Sent into ${chalk.blue(`udp://${host}:${port}`)} the following data:\n${chalk.keyword('orange')(data)}\n################################`));
  });
});
