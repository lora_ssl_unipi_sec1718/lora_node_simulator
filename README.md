# LoraWan node & gateway simulator
Generating LoraWan and Semtech Udp packet and sends them to the network server according to LoraWan **1.0.x** spec.
Usefull to develop a custom application that reads and sends data from/to a LoraWan network.

## Packet generation sequence

The packet generation follows these steps:
1. LoraWan packet generation
2. Base64 encode the result from step above
3. Generate a Semtech UDP json content using the result from above.
4. Send to the LoraWan Network server.

## Configuration file
The configuration contains this content:

```json
{
  "lorawan":{
    "appSKey":^application session key^,
    "ntwSKey":^network session key^,
    "nodeId": ^node id^
  },
  "udp":{
    "host": ^network server host^,
    "port": ^network server port^,
  }
}

```
